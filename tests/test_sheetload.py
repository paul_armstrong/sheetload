#!/usr/bin/env python

"""Tests for `sheetload` package."""

import pytest


from sheetload import sheetload


@pytest.fixture
def response():
    """Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    # import requests
    # return requests.get('https://gitlab.com/audreyr/cookiecutter-pypackage')


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument."""
    # from bs4 import BeautifulSoup
    # assert 'gitlab' in BeautifulSoup(response.content).title.string
